from scapy.all import *
import optparse
import sys
import datetime

def banner():
    print "   ______            __             __   ____                                 _      "
    print "  / ____/___  ____  / /__________  / /  / __ \________  ________  ____  _____(_)___ _"
    print " / /   / __ \/ __ \/ __/ ___/ __ \/ /  / /_/ / ___/ _ \/ ___/ _ \/ __ \/ ___/ / __ `/"
    print "/ /___/ /_/ / / / / /_/ /  / /_/ / /  / ____/ /  /  __(__  )  __/ / / / /__/ / /_/ / "
    print "\____/\____/_/ /_/\__/_/   \____/_/  /_/   /_/   \___/____/\___/_/ /_/\___/_/\__,_/  "
    print "                                                                                     "
    print "+-------------------------------------------------------------+"
    print '|  SCRIPT: controlPresencia.py v.:1.0                         |'
    print '|  LICENSE: GPL-2.0                                           |'
    print '|  ORIGINAL AUTHOR: (madyyelf) Raul Gimenez Herrada           |'
    print '|  WEB: http://alquimiabinaria.cat EMAIL: madyyelf@gmail.com  |'
    print "+-------------------------------------------------------------+"
    print "                                                                                     "


class person():
    def __init__(self,firstName=None,lastName=None,mac=None):
        self.firstName=firstName
        self.lastName=lastName
        self.mac=mac
        self.lastSee=None
        self.here=False

class group():
    def __init__(self,fileGroup):
        self.file=fileGroup
        self.persons=[]
    def loadGroup(self):
        try:
            personList=open(self.file)
        except ValueError:
            print "[!] ERROR: Error opening:",self.file
            exit(0)

        guy=person()

        try:
            for line in personList:
                (mac,lastName,firstName)=line.split(",")
                guy=person(firstName.rstrip('\n'),lastName,mac)
                self.persons.append(guy)
        except ValueError:
            print "[!] ERROR: File without proper format."
            exit(0)

    def listPersons(self):
        for guy in self.persons:
            print "Person:",guy.firstName,guy.lastName

class MACscanner():
    def __init__(self,interface,timeout):
        self.interface = interface
        self.timeout=timeout
        self.observedclients = []



    def __sniffmgmt__(self,p):
        self.stamgmtstypes = (0, 2, 4)

        if p.haslayer(Dot11):
            if p.type == 0 and p.subtype in self.stamgmtstypes:
                if p.addr2 not in self.observedclients:
                    self.observedclients.append(p.addr2)
    def run(self):
        del self.observedclients[:]
        try:
            sniff(iface=self.interface, prn=self.__sniffmgmt__,timeout=self.timeout*60)
        except ValueError:
            print "[!] ERROR: Sniffer returns:",ValueError
            exit(0)

def main():
    parser=optparse.OptionParser('Usage: python controlPresencia.py -l <list_file> [-r <refresh_rate>] [-i <interface>]')
    parser.add_option('-l',dest='listFile',type='string',help='List with mac, first name and last name separated with comas.')
    parser.add_option('-r',dest='refreshRate',type='int',default="5",help='Refresh rate in minutes. Default 5 minutes.')
    parser.add_option('-i',dest='interface',type='string',default="mon0",help='Interface in monitor mode.  Default mon0.')

    (options,args)=parser.parse_args()
    listFile=options.listFile
    refreshRate=options.refreshRate
    interface=options.interface

    banner()

    if listFile == None:
        print parser.usage
        exit(0)

    watchGroup=group(listFile)
    watchGroup.loadGroup()

    sniffer=MACscanner(interface,refreshRate)

    print "[+] Sniffing WiFi probes..."

    while True:
        time=datetime.datetime.now()
        sniffer.run()

        for person in watchGroup.persons:
            for mac in sniffer.observedclients:
                if person.mac==mac :
                    person.lastSee=time
                    if person.here==False:
                        person.here=True
                        print "<"+str(time)+">:",person.lastName+",",person.firstName,"has arrived."
        
            if person.lastSee!=None and person.lastSee<time:
                presence=(time-person.lastSee).total_seconds()/60
                if presence>1:
                    if person.here==True:
                        print "<"+str(time)+">:",person.lastName+",",person.firstName,"has gone."
                        person.here=False
 
if __name__=='__main__':
    main()
